const axios = require('axios');
const Discord = require('discord.js');
const client = new Discord.Client();

const config = require('./config');
const constants = require('./src/constants');

client.on('ready', () => {
  console.log('MTG Bot is ready!');
});

client.on('message', msg => {
  if (msg.content.includes(constants.cmd_delimiter)) {
    // handle command
    // could be could be a scryfall query or just bot commands to restart or kill the bot
    console.log('handling command:', msg.content);
    handleCommand(msg.channel, msg.content);
  } else if (/\[[\w\s,]*\]*/.test(msg.content)) {
    // requesting card image(s) in format [card name]
  }
});

function handleCommand(channel, command) {
  if (command.includes(constants.scryfall_prefix)) {
    // scryfall query
    scry(channel, command.substring(6));
  } else {
    // basically every other command (ex. restart or kill)
    switch (command.toUpperCase()) {
      case `${constants.cmd_delimiter}${constants.destroy}`:
        destroy(channel);
        break;
      case `${constants.cmd_delimiter}${constants.restart}`:
        restart(channel);
        break;
    }
  }
}

async function scry(channel, params) {
  let cards = await axios.get(`${constants.scryfall_api}/cards/search?q=${params}`);
  if (!cards.data.has_more) {
    const embed = new Discord.RichEmbed()
      .setTitle(cards.data.data[0].name)
      .setDescription(cards.data.data[0].oracle_text)
      .setColor(8344826)
      .setTimestamp()
      .setURL(cards.data.data[0].scryfall_uri)
      .setImage(cards.data.data[0].image_uris.png)
      .addField('_Set_', cards.data.data[0].set.toUpperCase())
      .addField('_Flavor Text_', cards.data.data[0].flavor_text)
      .addField('_Price (USD)_', `$${cards.data.data[0].usd}`);
    await channel.send({embed});
  } else {
  }
}

function restart(channel) {
  channel
    .send('Restarting bot...')
    .then(msg => client.destroy())
    .then(() => client.login(config.token));
}

function destroy(channel) {
  channel.send('Destroying bot...').then(msg => client.destroy());
}

client.login(config.token);
