const COMMAND_DELIMITER = '!';
const RESTART = 'RESTART';
const DESTROY = 'DESTROY';

const SCRYFALL_PREFIX = 'scry';
const SCRYFALL_API = 'https://api.scryfall.com';

module.exports = {
  scryfall_prefix: SCRYFALL_PREFIX,
  scryfall_api: SCRYFALL_API,
  cmd_delimiter: COMMAND_DELIMITER,
  restart: RESTART,
  destroy: DESTROY,
};
